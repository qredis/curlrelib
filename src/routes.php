<?php

use Illuminate\Support\Facades\Route;

Route::post('/frontend/file', 'Qredis\Curlrelib\Http\Controllers\CryptoController@decryptAndGetFile');
Route::get('/frontend/unlink', 'Qredis\Curlrelib\Http\Controllers\CryptoController@unlinkFile');
